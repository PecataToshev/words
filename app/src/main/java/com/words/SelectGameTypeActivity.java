package com.words;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.words.utils.BasicFunctions;

public class SelectGameTypeActivity extends AppCompatActivity {
    Button button_localGame;
    Button button_computer;
    Button button_internet;
    Button button_bluetooth;

    private void initialise() {
        button_localGame = (Button)findViewById(R.id.button_localGame);
        button_computer  = (Button)findViewById(R.id.button_computer);
        button_internet  = (Button)findViewById(R.id.button_internet);
        button_bluetooth = (Button)findViewById(R.id.button_bluetooth);
    }

    public void dialogMessage(String alertMessage){
        String okMessage = getString(R.string.OK);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(alertMessage)
                .setCancelable(false)
                .setPositiveButton(okMessage, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        localsGame(true);
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void localsGame(boolean playComputer){
        Intent intent = new Intent(this, InputNamesLocalActivity.class);
        startActivity(intent);
        intent.putExtra(getString(R.string.activityExtra_playsComputer), playComputer);
        startActivity(intent);
        finish();
    }

    private void isntReady(){
        BasicFunctions.dialogMessage(SelectGameTypeActivity.this, getString(R.string.IsntReadyYet));
    }

    @Override
    public void onBackPressed(){
        BasicFunctions.toMainMenu(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_game_type);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        initialise();

        button_localGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                localsGame(false);
            }
        });

        button_computer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogMessage(getString(R.string.ComputerAddAfterAll));
            }
        });

        button_internet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isntReady();
            }
        });

        button_bluetooth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isntReady();
            }
        });
    }
}
