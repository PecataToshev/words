package com.words;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.widget.SeekBar;

import com.words.users.SelectLogOrRegActivity;
import com.words.utils.BasicFunctions;

public class SettingsActivity extends PreferenceActivity {
    Preference username;
    Preference turn;
    Preference autoAdd;
    AlertDialog.Builder popDialog;
    int value = 0;

    private void initialise(){
        value = BasicFunctions.getTurnTime(this);

        username = (Preference) findPreference(getString(R.string.pref_username));
        username.setSummary(BasicFunctions.getUsername(this));

        turn = (Preference) findPreference(getString(R.string.pref_turnTime));
        turn.setSummary(BasicFunctions.getTurnTime(this) + " " + getString(R.string.seconds));

        autoAdd = (Preference) findPreference(getString(R.string.pref_addName));
        autoAdd.setDefaultValue(false);
    }

    @Override
    public void onBackPressed(){
        BasicFunctions.toMainMenu(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.settings);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        initialise();

        username.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {

                BasicFunctions.startNewActivity(SettingsActivity.this, SelectLogOrRegActivity.class);

                return false;
            }
        });

        turn.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                ShowDialog();
                return false;
            }
        });

        autoAdd.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {

                SharedPreferences.Editor editor = BasicFunctions.prefsEditor(SettingsActivity.this);
                editor.putBoolean(getString(R.string.pref_addName), (Boolean) newValue);
                editor.apply();

                return true;
            }
        });
    }

    public void ShowDialog(){
        value = BasicFunctions.getTurnTime(this);
        popDialog = new AlertDialog.Builder(this);
        final SeekBar seek = new SeekBar(this);
        seek.setMax(90);

        popDialog.setTitle(R.string.turnLongText);
        popDialog.setView(seek);
        seek.setProgress(value - 30);


        seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                //Do something here with new value
                value = progress;
            }

            public void onStartTrackingTouch(SeekBar arg0) {

            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                BasicFunctions.toast(SettingsActivity.this, (value + 30 + " " + getString(R.string.seconds)));
            }

        });

        popDialog.setPositiveButton(R.string.change, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {

                SharedPreferences.Editor editor = BasicFunctions.prefsEditor(SettingsActivity.this);
                editor.putInt(getString(R.string.pref_turnTime), (value + 30));
                editor.apply();
                turn.setSummary(BasicFunctions.getTurnTime(SettingsActivity.this) + " " + getString(R.string.seconds));


                dialog.dismiss();
            }

        });

        popDialog.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }

        });

        popDialog.create();
        popDialog.show();
        }


    }
