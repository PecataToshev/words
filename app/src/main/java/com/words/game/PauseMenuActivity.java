package com.words.game;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.words.R;

public class PauseMenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two_buttons);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Button mainMenu = (Button) findViewById(R.id.button2);
        Button resume   = (Button) findViewById(R.id.button1);
        TextView tv = (TextView) findViewById(R.id.pausedGame);
        tv.setVisibility(View.VISIBLE);
        mainMenu.setText(getString(R.string.toMainMenu));
        resume.setText(getString(R.string.continueGame));
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)resume.getLayoutParams();
        params.setMargins(0, ((int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 97, getResources().getDisplayMetrics())),
                0, 0);
        resume.setLayoutParams(params);


        mainMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(false);
            }
        });

        resume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(true);
            }
        });
    }

    private void setResult(boolean x){
        Intent intent = new Intent();
        intent.putExtra("plays", Boolean.toString(x));
        setResult(RESULT_OK, intent);
        finish();
    }
}
