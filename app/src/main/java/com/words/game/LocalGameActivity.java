package com.words.game;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.words.R;
import com.words.utils.BasicFunctions;

import java.util.ArrayList;


public class LocalGameActivity extends AppCompatActivity {
    private EditText inputWord;
    private Button inputButton;
    private ImageButton voiceInput;
    protected static final int RESULT_SPEECH = 0;
    protected static final int RESULT_MENU = 1;
    private Game game;
    private boolean pausedGame;
    private boolean pauseMenuWasShowed;
    private boolean forVoiceInput;

    private void initialise() {
        pausedGame = false;
        pauseMenuWasShowed = false;
        forVoiceInput = false;

        inputWord = (EditText) findViewById(R.id.inputWord);
        inputButton = (Button) findViewById((R.id.inputButton));
        voiceInput = (ImageButton) findViewById(R.id.microphoneInput);

        game = new Game(this, getIntent().getExtras().getString(getString(R.string.activityExtra_names)),
                getIntent().getExtras().getInt(getString(R.string.activityExtra_compLvl), 0));
    }

    private void startPauseMenu() {
        pauseMenuWasShowed = true;
        Intent intent = new Intent(this, PauseMenuActivity.class);
        startActivityForResult(intent, RESULT_MENU);
    }

    private void pauseGame() {
        pausedGame = true;
        game.setIsPaused(true);
    }

    private InputMethodManager inputMethodManager(){
        return (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
    }

    private void hideKeyboard(){
        inputMethodManager().hideSoftInputFromWindow(inputWord.getWindowToken(), 0);
    }

    private void showKeyboard(){
        inputWord.requestFocus();
        inputMethodManager().toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    @Override
    protected void onPause() {
        super.onPause();

        hideKeyboard();

        if (forVoiceInput) {
            return;
        }

        pauseGame();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (forVoiceInput) {
            forVoiceInput = false;
            return;
        }

        if (pausedGame && !pauseMenuWasShowed) {
            pauseMenuWasShowed = true;
            startPauseMenu();
            return;
        }

        if (pausedGame && pauseMenuWasShowed) {
            pausedGame = false;
            pauseMenuWasShowed = false;
        }

        showKeyboard();
    }

    @Override
    public void onBackPressed() {
        pauseGame();
        startPauseMenu();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_for_local_games);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        initialise();
        game.startTimer();

        inputButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showKeyboard();
                String word = inputWord.getText().toString().toLowerCase();
                if (game.checkWord(word)) {
                    game.stopTimer();
                }

            }
        });

        voiceInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                promptSpeechInput();
            }
        });
    }

    private void promptSpeechInput() {
        forVoiceInput = true;
        inputWord.setText("");

        BasicFunctions.promptSpeechInput(this, RESULT_SPEECH);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.voice, menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_SPEECH && resultCode == RESULT_OK && null != data) {
            ArrayList<String> text = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            inputWord.setText(text.get(0));
            inputButton.performClick();

            return;
        }

        if (requestCode == RESULT_MENU && resultCode == RESULT_OK  && null != data) {
            boolean plays = Boolean.parseBoolean(data.getStringExtra("plays"));

            game.setIsPaused(false);
            if (!plays) {
                final Activity activity = this;
                AlertDialog.Builder builder = new AlertDialog.Builder(this);

                builder.setMessage(R.string.toMainMenuExitGame);

                builder.setPositiveButton(R.string.Yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        game.setIsPaused(true);
                        BasicFunctions.toMainMenu(activity);
                    }

                });

                builder.setNegativeButton(R.string.No, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }

                });

                AlertDialog alert = builder.create();
                alert.show();
            }

            return;
        }


    }
}
