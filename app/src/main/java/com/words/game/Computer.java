package com.words.game;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

import com.words.utils.BasicFunctions;
import com.words.utils.WordsContainer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

public class Computer extends AsyncTask<String, Void, String> {
    ArrayList <WordsContainer> wordsList;
    private Button inputButton;
    private EditText inputWord;
    String word = "";

    public Computer(EditText _inputWord, Button _inputButton){
        inputWord = _inputWord;
        inputButton = _inputButton;
    }

    public void setWordsList(ArrayList<WordsContainer> _wordsList) {
        wordsList = _wordsList;
    }

    private boolean checkForExist(String word){
        for(int i = 0;  i <= wordsList.size()/2;  i++){
            if(word.equals(wordsList.get(i).getWord())){
                return true;
            }

            if(word.equals(wordsList.get(wordsList.size() - i - 1).getWord())){
                return true;
            }

        }

        return false;
    }

    private String wordsFromStringToJSON(String strJson) throws JSONException {
        JSONObject jsonWords = new JSONObject(strJson);
        JSONArray jsonArray = jsonWords.optJSONArray("words");

        for(int i = 0;  i < jsonArray.length();  i++){
            if(!checkForExist(jsonArray.getJSONObject(i).getString("word"))){
                return jsonArray.getJSONObject(i).getString("word");
            }
        }
        return "";
    }

    @Override
    protected String doInBackground(String... params) {
        String requestURL = BasicFunctions.URL() + "GetWordsList?firstLeter=";
        int letterNumber = Arrays.asList(BasicFunctions.allBulAlphabet()).indexOf(params[0]);
        letterNumber++;
        requestURL += letterNumber;
        requestURL += "&level=";
        requestURL += params[1];

        try {
            word = wordsFromStringToJSON(BasicFunctions.getResult(requestURL));
            if(Integer.parseInt(params[1]) == 5 && word.equals("")){
                doInBackground(params);
            }

        } catch (JSONException e) {
            Log.e("Error", "IOException", e);
        }

        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        inputWord.setText(word);
        inputButton.performClick();
    }
}
