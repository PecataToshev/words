package com.words.game;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.CountDownTimer;
import android.util.TypedValue;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.words.MainActivity;
import com.words.R;
import com.words.users.UploadPoints;
import com.words.utils.BasicFunctions;
import com.words.utils.PlayerContainer;
import com.words.utils.WordsContainer;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;

public class Game {
    private TextView userName;
    private TextView timerShow;
    private LinearLayout wordsContainer;
    private EditText inputWord;
    private String playersUnlisted;
    private ArrayList<PlayerContainer> players;
    private Button inputButton;
    private ImageButton voiceInput;
    private int currentUser;
    private ArrayList<WordsContainer> wordsList;
    private boolean timerManuallyTimedOut;
    private Activity activity;
    private String bulgarianAlphabetGlasni[];
    private String bulgarianAlphabetSaglasni[];
    private boolean stillPlaying;
    private TextView lastLetterHold;
    private int compLvl;
    private CountDownTimer gameTimer;
    private long total;
    private boolean isPaused;
    private long turnLong;
    private boolean gameWasPaused;
    private Computer comp;

    public Game(Activity _activity, String _playersUnlisted, int _compLvl){
        initialise(_activity);
        playersUnlisted = _playersUnlisted;
        modifyPlayers(playersUnlisted);
        compLvl = _compLvl;

        getFirstChar();
        putIntoTextView(wordsList.get(0).getWord());


        userName.setText(players.get(0).getName());
    }

    public void initialise(Activity _activity){//initialises xml things and other gadgets
        activity = _activity;
        timerManuallyTimedOut = false;
        isPaused = false;
        gameWasPaused = false;


        players   = new ArrayList<PlayerContainer>();
        wordsList = new ArrayList<WordsContainer>();


        turnLong = BasicFunctions.getTurnTime(activity)*1000;
        total = turnLong;
        stillPlaying = true;

        inputWord            = (EditText)     this.activity.findViewById(R.id.inputWord);
        userName             = (TextView)     this.activity.findViewById((R.id.username));
        timerShow            = (TextView)     this.activity.findViewById(R.id.timerShow);
        wordsContainer       = (LinearLayout) this.activity.findViewById(R.id.wordsContainer);
        inputButton          = (Button)       this.activity.findViewById((R.id.inputButton));
        voiceInput           = (ImageButton)  this.activity.findViewById(R.id.microphoneInput);
        lastLetterHold       = (TextView)     this.activity.findViewById(R.id.lastLetter);

        String bulgarianAlphabetGl1[] = {"а","е","и","о","у","ъ"};
        String bulgarianAlphabetSa1[] = {"б","в","г","д","ж","з","й","к","л","м","н","п","р","с",
                "т","ф","х","ц","ч","ш","щ","ь","ю","я"};
        bulgarianAlphabetGlasni = bulgarianAlphabetGl1;
        bulgarianAlphabetSaglasni = bulgarianAlphabetSa1;
    }

    private void modifyPlayers(String str){//players from string to array list
        ArrayList<String> p = BasicFunctions.getStringToArrList(str);

        for (int i = 0;  i < p.size();  i++){
            players.add(new PlayerContainer(p.get(i)));
        }
    }

    private void getFirstChar(){//getting first letter (only when game is started)
        int start = new Random().nextInt(30);
        while (start == 27){
            start = new Random().nextInt(30);
        }

        WordsContainer z = new WordsContainer(BasicFunctions.allBulAlphabet()[start], -1);
        wordsList.add(z);
    }

    public void startTimer() {//starts the timer
        startCountDownTimer();
    }

    public void setIsPaused(boolean _isPaused) {//when game is paused or started after a pause
        isPaused = _isPaused;
        if (_isPaused){
            gameWasPaused = _isPaused;
            gameTimer.cancel();
        } else {
            startTimer();
        }
    }

    public void stopTimer() {//manually stops the timer (used when player word is inputted)
        timerManuallyTimedOut = true;
        gameTimer.cancel();
        gameTimer.onFinish();
    }

    private boolean checkForSpecialCharacters(String text){//return true if contains and false if don't
        boolean glasni = false;
        boolean saglas = false;
        int gla = 0;
        int sag = 0;

        ArrayList<String> gl = new ArrayList<String>(Arrays.asList(bulgarianAlphabetGlasni));
        ArrayList<String> sa = new ArrayList<String>(Arrays.asList(bulgarianAlphabetSaglasni));

        for(int i = 0;  i < text.length();  i++){
            String t = Character.toString(text.charAt(i));
            if(gl.contains(t) || t.equals("я")){
                gla++;
                sag = 0;
                glasni = true;
            } else if (sa.contains(t)){
                sag++;
                gla = 0;
                saglas = true;
            } else {
                BasicFunctions.dialogMessage(activity, activity.getString(R.string.wordsContainsSpecialCharacters));
                return true;
            }

            if(gla > 2){//more than 2 serial glasni
                BasicFunctions.dialogMessage(activity, "");
                return true;
            }

            if(sag > 5){//more than 5 serial saglasni
                BasicFunctions.dialogMessage(activity, "");
                return true;
            }

        }
        if(glasni && saglas){
            return false;
        }

        BasicFunctions.dialogMessage(activity, activity.getString(R.string.containsGlasniAndSaglasni));
        return true;
    }

    private boolean existingWordMessage(WordsContainer z){//it's boolean just to return it, nothing else
        String message =  activity.getString(R.string.usedWord) + " "
                + players.get(z.getId()).getName() + activity.getString(R.string.tryAgainWithOther);
        BasicFunctions.dialogMessage(activity, message);
        return false;
    }

    public boolean checkWord(String word){// check if word matches some check. if yes -> true, else false
        word = BasicFunctions.removeHomeAndEndSpaces(word);

        if(word.equals("")){
            return false;
        }

        if(word.length() > 39){
            BasicFunctions.dialogMessage(activity, activity.getString(R.string.largestWordInBulgarian));
            return false;
        }

        if(checkForSpecialCharacters(word)){
            return false;
        }
        
        if(word.length() < 3){
            BasicFunctions.dialogMessage(activity, activity.getString(R.string.atLeast3Characters));
            return false;
        }

        for(int i = 0;  i <= wordsList.size()/2;  i++) {
            if(word.equals(wordsList.get(i).getWord())) {
                return existingWordMessage(wordsList.get(i));
            }

            if(word.equals(wordsList.get(wordsList.size() - i - 1).getWord())){
                return existingWordMessage(wordsList.get(wordsList.size() - i - 1));
            }

        }

        Character lastlett = BasicFunctions.lastLetter(word);

        if(lastlett == 'ъ'){
            BasicFunctions.dialogMessage(activity, activity.getString(R.string.lastLetterEquals) + "ъ");
            return false;
        }

        if(lastlett == 'ь'){
            BasicFunctions.dialogMessage(activity, activity.getString(R.string.lastLetterEquals) + "ь");
            return false;
        }

        if(BasicFunctions.lastLetter(wordsList.get(wordsList.size() - 1).getWord()) != word.charAt(0)){
            BasicFunctions.dialogMessage(activity, activity.getString(R.string.lastWordChar));
            return false;
        }

        for(int i = 0;  i < word.length() - 3;  i++){
            if(word.charAt(i) == word.charAt(i + 1) && word.charAt(i + 1) == word.charAt(i + 2)){
                BasicFunctions.dialogMessage(activity, activity.getString(R.string.serialLetters));
                return false;
            }
        }

        wordsList.add(new WordsContainer(word, currentUser));
        putIntoTextView(word);

        return true;
    }

    private void putIntoTextView(String word){//puts newly added words in linear layout
        TextView wordShow = new TextView(this.activity);
        wordShow.setText(word);
        wordShow.setId(wordsList.size());
        wordShow.setLayoutParams(new
                ViewGroup.LayoutParams
                (ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        wordShow.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        wordsContainer.addView(wordShow, 0);
        lastLetterHold.setText(BasicFunctions.lastLetterString(word));
    }

    private void findNextPlayer(){//fins next playing player
        for (currentUser = (currentUser + 1)% players.size(); !players.get(currentUser % players.size()).isPlaying();  currentUser++){
            currentUser = currentUser % players.size();
        }
    }

    private void addPointToCurrentUser(){//adds point if word is inputted and it matches checks
        if(timerManuallyTimedOut){
            players.get(currentUser).setPoints(players.get(currentUser).getPoints() + 1);
        }
    }

    private void setUsername(String name){//shows in TextView current user name
        userName.setText(name);
    }

    private void changePlayer(){//gives next player
        inputWord.setText("");
        findNextPlayer();
        setUsername(players.get(currentUser).getName());
    }

    private int getPlayingPlayersNumber() {//return the number of still playing players
        int num = 0;

        for(int i = 0;  i < players.size();  i++) {
            if(players.get(i).isPlaying()){
                num++;
            }
        }
        return num;
    }

    private void computerTurn(){//computer plays
        inputWord.setEnabled(false);
        voiceInput.setEnabled(false);
        inputButton.setEnabled(false);


        comp = new Computer(inputWord, inputButton);
        comp.setWordsList(wordsList);


        String letter = wordsList.get(wordsList.size() - 1).getWord();
        letter = BasicFunctions.lastLetterString(letter);
        String lvlComp = Integer.toString(compLvl);


        comp.execute(letter, lvlComp);
    }

    private void onTimerFinish(){//when the timer is finished
        if(isPaused){
            return;
        }

        addPointToCurrentUser();
        inputWord.setText(null);
        inputWord.setEnabled(true);
        inputButton.setEnabled(true);
        voiceInput.setEnabled(true);

        if(!stillPlaying){
            onFinishedGame();
        } else {

            total = turnLong;

            if(gameWasPaused){
                startCountDownTimer();
                gameWasPaused = false;
            } else {
                gameTimer.start();
            }

            timerManuallyTimedOut = changePlayerOnTimerFinish();

            if (players.get(currentUser).getName().equals(activity.getString(R.string.Computer))
                    && players.get(currentUser).isPlaying()) {

                computerTurn();
            }
        }
    }

    private boolean changePlayerOnTimerFinish() {//changes players when timer is finished
        int removePlayerID = currentUser;

        if(!timerManuallyTimedOut){
            BasicFunctions.toast(activity, activity.getString(R.string.player)
                    + players.get(removePlayerID).getName()
                    + activity.getString(R.string.droppedOutOfTheGame));

            players.get(removePlayerID).stopPlaying();

            if(getPlayingPlayersNumber() < 2) {
                if(!stillPlaying) {
                    onFinishedGame();
                    return false;
                }
                stillPlaying = false;
            }
        }

        changePlayer();
        return false;
    }

    private void startCountDownTimer() {//timer is being initialised and started
        gameTimer = new CountDownTimer(total, 1000) {
            public void onTick(long millisUntilFinished) {
                total = millisUntilFinished;
                timerShow.setText(Long.toString(millisUntilFinished / 1000));
            }

            public void onFinish() {
                onTimerFinish();
            }
        }.start();
    }

    private void onFinishedGame(){//when the game is finished
        ArrayList<PlayerContainer> p = players;

        Collections.sort(players, new Comparator<PlayerContainer>() {
            public int compare(PlayerContainer a, PlayerContainer b) {
                if (a.getPoints() > b.getPoints()) {
                    return -1;
                } else if (a.getPoints() < b.getPoints()){
                    return 1;
                } else {
                    return a.getName().compareTo(b.getName());
                }
            }
        });

        String str = activity.getString(R.string.inputedWords) + " " + (wordsList.size() - 1) + "\r\n";


        for (int i = 0; i < p.size(); i++) {
            str += "\r\n" +  (i + 1) + ". " + p.get(i).getName() + " = " + p.get(i).getPoints();
        }

        str += "\r\n";
        gameTimer.cancel();
        dialogMessageEng(str);

        try {
            UploadPoints uploadPoints = new UploadPoints(activity, players);
            uploadPoints.execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void dialogMessageEng(String alertMessage){//dialog with player ratings
        gameTimer.cancel();
        String okMessage = activity.getString(R.string.OK);
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(alertMessage)
                .setCancelable(false)
                .setPositiveButton(okMessage, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        yesNoDialogInEnd();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void yesNoDialogInEnd() {//play again ot go to main menu
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        builder.setMessage(R.string.PlayAgainWithThesePeople);

        builder.setPositiveButton(R.string.Yes, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent localGame = new Intent(activity, activity.getClass());
                localGame.putExtra("names", playersUnlisted);
                localGame.putExtra("compLvl", compLvl);
                activity.startActivity(localGame);
                activity.finish();
            }

        });

        builder.setNegativeButton(R.string.No, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent main = new Intent(activity, MainActivity.class);
                activity.startActivity(main);
                activity.finish();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }
}
