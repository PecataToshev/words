package com.words;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.words.users.UploadPoints;
import com.words.utils.BasicFunctions;

public class MainActivity extends AppCompatActivity {

    Button button_startGame;
    Button button_settings;
    Button button_about;

    private void initialise() {
        button_startGame = (Button)findViewById(R.id.button_selectGame);
        button_settings = (Button)findViewById(R.id.button_settings);
        button_about = (Button)findViewById(R.id.button_about);
    }

    @Override
    public void onBackPressed(){
        BasicFunctions.exitApp(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        initialise();

        button_startGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BasicFunctions.startNewActivity(MainActivity.this, SelectGameTypeActivity.class);
            }
        });

        button_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BasicFunctions.startNewActivity(MainActivity.this, SettingsActivity.class);
            }
        });

        button_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                BasicFunctions.startNewActivity(MainActivity.this, About.class);
            }
        });

        if(!BasicFunctions.sharedPrefs(this).getString(getString(R.string.pref_players), "").isEmpty()){
            UploadPoints uploadPoints = new UploadPoints(this);
            uploadPoints.execute();
        }
    }
}