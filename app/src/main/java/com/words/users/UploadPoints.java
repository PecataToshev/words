package com.words.users;


import android.app.Activity;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.words.R;
import com.words.utils.BasicFunctions;
import com.words.utils.PlayerContainer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class UploadPoints extends AsyncTask<String, Void, String> {
    Activity activity;

    public UploadPoints(Activity activity) {
        this.activity = activity;
    }

    public UploadPoints (Activity activity, ArrayList<PlayerContainer> players) throws JSONException {
        this.activity = activity;
        JSONArray ja = new JSONArray();
        for(int i = 0;  i < players.size();  i++){
            PlayerContainer p = players.get(i);

            JSONObject jo = new JSONObject();
            if(p.getName().equals(BasicFunctions.getUsername(activity))){
                jo.put(activity.getString(R.string.pref_email), BasicFunctions.getEmail(activity));
            } else {
                jo.put(activity.getString(R.string.pref_email), p.getEmail());
            }

            jo.put(activity.getString(R.string.pref_points), p.getPoints());

            if(p.haveEmail()){
                ja.put(jo);
            }
        }

        JSONObject mainObj = new JSONObject();
        mainObj.put(activity.getString(R.string.pref_players), ja);

        setPrefs(mainObj.toString());
    }

    private void setPrefs(String text){
        SharedPreferences.Editor editor = BasicFunctions.prefsEditor(activity);
        editor.putString(activity.getString(R.string.pref_players), text);
        editor.apply();
    }

    private JSONArray getJSON () throws JSONException {
        String strJson = BasicFunctions.sharedPrefs(activity).getString(activity.getString(R.string.pref_players), "");
        return new JSONObject(strJson).optJSONArray(activity.getString(R.string.pref_players));
    }

    private JSONArray removeJSONObject (JSONArray ja, int pos){
        JSONArray nja = new JSONArray();
        try{
            for(int i = 0;  i < ja.length();  i++){
                if(i != pos) {
                    nja.put(ja.get(i));
                }

            }
        }catch (Exception e){e.printStackTrace();}

        return nja;
    }

    private String sendToDB (String email, String points){
        String requestURL = BasicFunctions.URL() + "UpdatePoints?";
        requestURL += "email=" + email;
        requestURL += "&point=" + points;

        return BasicFunctions.getResult(requestURL);
    }

    @Override
    protected String doInBackground(String... params) {
        JSONArray playersArray;
        try {
             playersArray = getJSON();
            for(int i = 0;  i < playersArray.length();  i++){

                JSONObject p = playersArray.getJSONObject(i);
                switch (sendToDB(p.getString(activity.getString(R.string.pref_email)),
                        p.getString(activity.getString(R.string.pref_points)))){
                    case "successful":case "nope":
                        playersArray = removeJSONObject(playersArray, i);
                        i--;
                        break;
                    case "unsuccessful": default:
                        break;
                }

            }

            if(playersArray.length() == 0){
                setPrefs("");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(String result){

    }
}
