package com.words.users;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.words.R;
import com.words.utils.BasicFunctions;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;

public class UserLoginRegister extends AsyncTask<String, Void, String> {
    private Activity activity;
    private String requestURL;
    private ProgressDialog loading;
    private String resultJSON;
    private String eMail;

    public UserLoginRegister(Activity activity) {
        this.activity = activity;
        loading = null;
    }

    public void createLink(String email, String password) {//Login
        requestURL = BasicFunctions.URL();
        requestURL += "Login?email=" + email + "&password=" + hashPassword(email, password);
        eMail = email;
    }

    public void createLink(String email, String password, String username){//Register
        requestURL = BasicFunctions.URL();
        requestURL += "Register?email=" + email + "&password=" + hashPassword(email, password) +
                "&username=" + username;
        eMail = email;
    }

    public static boolean checkEmail(String email){
        // should return false when email isn't correct and true if it's correct

        if(!email.contains("@")){
            return false;
        }

        String after = email.substring(email.indexOf("@") + 1);
        if(!after.contains(".") || after.length() < 4){
            return false;
        }

        if(!BasicFunctions.lastLetterString(after).matches("[a-zA-Z]") &&
                !Character.toString(after.charAt(0)).matches(".*[a-zA-Z]+.*")){

            return false;
        }

        return true;
    }

    public boolean checkPassword(String password){
        // should return false when pass doesn't matches password criteria and true when it does

        if(password.length() < 6){
            return false;
        }

        if(password.contains(" ")){
            return false;
        }

        return true;
    }

    private String hashPassword(String salt, String password){
        try{
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest((password + salt).getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if(hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }

    private void setID(String strID){
        int id = Integer.parseInt(strID);
        SharedPreferences.Editor editor = BasicFunctions.prefsEditor(activity);
        editor.putInt(activity.getString(R.string.pref_userID), id);
        editor.apply();
    }

    private void setUsername(String name){
        SharedPreferences.Editor editor = BasicFunctions.prefsEditor(activity);
        editor.putString(activity.getString(R.string.pref_username), name);
        editor.apply();
    }

    private void setPoints(String strEXP){
        SharedPreferences.Editor editor = BasicFunctions.prefsEditor(activity);
        editor.putLong(activity.getString(R.string.pref_points), Long.parseLong(strEXP));
        editor.apply();
    }

    private void setEmail(String email){
        SharedPreferences.Editor editor = BasicFunctions.prefsEditor(activity);
        editor.putString(activity.getString(R.string.pref_email), email);
        editor.apply();
    }

    @Override
    protected void onPreExecute(){//before do in back
        super.onPreExecute();

        loading = new ProgressDialog(activity);
        loading.setCancelable(true);
        loading.setMessage(activity.getString(R.string.connecting) + "...");
        loading.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        loading.show();
    }

    @Override
    protected String doInBackground(String... params) {
        //for connecting to db

        resultJSON = BasicFunctions.getResult(requestURL);

        return null;
    }
    
    private int check(){
        if (resultJSON.isEmpty()) {//error when registering user
            BasicFunctions.toast(activity, activity.getString(R.string.sthWentWrong));
            return 0;
        }

        if (resultJSON.equals("unsuccessful") || resultJSON.equals("paramsReg")) {//error when registering user
            BasicFunctions.toast(activity, activity.getString(R.string.sthWentWrong));
            return 0;
        }

        if(resultJSON.equals("exist")){//this email exist
            BasicFunctions.toast(activity, activity.getString(R.string.existingUser));
            return 0;
        }

        if(resultJSON.equals("successful")){//successful registration
            BasicFunctions.toast(activity, activity.getString(R.string.successfulReg));
            return 2;
        }

        if(resultJSON.equals("nope")){//invalid email or password
            BasicFunctions.toast(activity, activity.getString(R.string.invalid_email));
            return 0;
        }

        return 1;
    }

    @Override
    protected void onPostExecute(String result) {
        //after do in back
        int ch = check();
        loading.dismiss();

        if(ch == 1){//logged in
            try {
                JSONObject jsonUser = new JSONObject(resultJSON);
                setUsername(jsonUser.getString("username"));
                setID(jsonUser.getString("id"));
                setPoints(jsonUser.getString("exp"));
                setEmail(eMail);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if(ch == 2) { //registered
            BasicFunctions.toast(activity, activity.getString(R.string.successfulRegistration));
            BasicFunctions.startNewActivity(activity, LoginActivity.class);
        }
    }
}
