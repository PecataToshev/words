package com.words.users;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.words.R;
import com.words.SettingsActivity;
import com.words.utils.BasicFunctions;

public class LoginActivity extends AppCompatActivity {
    EditText emailCont;
    EditText passCont;
    String email;
    String pass;
    Button login;
    UserLoginRegister userLoginRegister;

    private void initialise(){
        emailCont = (EditText) findViewById(R.id.emailLog);
        passCont = (EditText) findViewById(R.id.passLog);
        login = (Button) findViewById(R.id.login);

        email = "";
        pass = "";
    }

    private void getFieldsText(){
        email = emailCont.getText().toString();
        pass = passCont.getText().toString();

        email = BasicFunctions.removeHomeAndEndSpaces(email);
    }

    private void validate(){
        getFieldsText();

        userLoginRegister = new UserLoginRegister(this);

        if(email.isEmpty() || pass.isEmpty()){
            BasicFunctions.toast(this, "Всички полета са ЗАДЪЛЖИТЕЛНИ!");
            return;
        }

        if(email.contains(" ") || pass.contains(" ")){
            BasicFunctions.toast(this, "Не се допускат празни пространства (спейсове)");
            return;
        }

        if(!UserLoginRegister.checkEmail(email)){
            BasicFunctions.toast(this, "Моля въведете валиден Email адрес!");
            return;
        }

        if(!userLoginRegister.checkPassword(pass)){
            BasicFunctions.toast(this, getString(R.string.invalid_password));
            return;
        }

        userLoginRegister.createLink(email, pass);
        userLoginRegister.execute();

    }

    @Override
    public void onBackPressed(){
        BasicFunctions.startNewActivity(this, SettingsActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        initialise();

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validate();
            }
        });
    }
}
