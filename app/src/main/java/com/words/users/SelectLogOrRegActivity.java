package com.words.users;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.words.R;
import com.words.SettingsActivity;
import com.words.utils.BasicFunctions;

public class SelectLogOrRegActivity extends AppCompatActivity {

    @Override
    public void onBackPressed(){
        BasicFunctions.startNewActivity(this, SettingsActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two_buttons);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Button log = (Button) findViewById(R.id.button2);
        Button reg   = (Button) findViewById(R.id.button1);
        TextView tv = (TextView) findViewById(R.id.textOr);
        tv.setVisibility(View.VISIBLE);
        reg.setText(getString(R.string.register));
        log.setText(getString(R.string.sign_in));
        reg.setTextSize(TypedValue.COMPLEX_UNIT_SP, 19);
        log.setTextSize(TypedValue.COMPLEX_UNIT_SP, 19);

        log.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BasicFunctions.startNewActivity(SelectLogOrRegActivity.this, LoginActivity.class);
            }
        });

        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BasicFunctions.startNewActivity(SelectLogOrRegActivity.this, RegisterActivity.class);
            }
        });
    }
}
