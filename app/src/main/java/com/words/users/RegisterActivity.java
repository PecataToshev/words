package com.words.users;

import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.words.R;
import com.words.SettingsActivity;
import com.words.utils.BasicFunctions;

public class RegisterActivity extends AppCompatActivity {
    EditText emailCont;
    EditText usernameCont;
    EditText passCont;
    EditText passRepeatCont;
    Button register;
    String email;
    String username;
    String pass;
    String passRepeat;
    UserLoginRegister userLoginRegister;

    private void initialise(){
        emailCont = (EditText) findViewById(R.id.emailReg);
        usernameCont = (EditText) findViewById(R.id.usernameReg);
        passCont = (EditText) findViewById(R.id.passReg);
        passRepeatCont = (EditText) findViewById(R.id.passRepeatReg);
        register = (Button) findViewById(R.id.register);

        email = "";
        username = "";
        pass = "";
        passRepeat = "";
    }

    private void getFieldsText(){
        email = emailCont.getText().toString();
        username = usernameCont.getText().toString();
        pass = passCont.getText().toString();
        passRepeat = passRepeatCont.getText().toString();

        email = BasicFunctions.removeHomeAndEndSpaces(email);
        username = BasicFunctions.removeHomeAndEndSpaces(username);
    }

    private void validate(){
        getFieldsText();

        userLoginRegister = new UserLoginRegister(this);

        if(email.isEmpty() || username.isEmpty() || pass.isEmpty() || passRepeat.isEmpty()){
            BasicFunctions.toast(this, getString(R.string.allFieldsAreRequired));
            return;
        }

        if(email.contains(" ") || username.contains(" ") || pass.contains(" ") || passRepeat.contains(" ")){
            BasicFunctions.toast(this, getString(R.string.spacesAreNotAllowed));
            return;
        }

        if(username.length() < 4){
            BasicFunctions.toast(this, getString(R.string.usernameIsTooShort));
            return;
        }

        if(!UserLoginRegister.checkEmail(email)){
            BasicFunctions.toast(this, getString(R.string.inputValidEmail));
            return;
        }

        if(!userLoginRegister.checkPassword(pass)){
            BasicFunctions.toast(this, getString(R.string.invalid_password));
            return;
        }

        if(!pass.equals(passRepeat)){
            BasicFunctions.toast(this, getString(R.string.passwordsDoNotMatch));
            return;
        }

        userLoginRegister.createLink(email, pass, username);
        userLoginRegister.execute();

    }

    @Override
    public void onBackPressed(){
        BasicFunctions.startNewActivity(this, SettingsActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        initialise();

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validate();
            }
        });
    }
}
