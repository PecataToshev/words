package com.words.utils;

public class PlayerContainer {
    private String name;
    private int points;
    private boolean play;
    private String email;

    public PlayerContainer(String playerName){
        if(BasicFunctions.nameContainsEmail(playerName)){

            email = playerName.substring(0, playerName.indexOf("#"));
            name = BasicFunctions.getNameWithoutEmail(playerName);

        } else {

            name = playerName;
            email = null;

        }

        points = 0;
        play   = true;
    }

    public void setName(String playerName){
        name = playerName;
    }

    public void setPoints(int a){
        points = a;
    }

    public void stopPlaying(){
        play = false;
    }

    public String getName(){
        return name;
    }

    public int getPoints(){
        return points;
    }

    public boolean isPlaying() {
        return play;
    }

    public void setPlay(boolean play) {
        this.play = play;
    }

    public String getEmail() {
        return email;
    }

    public  boolean haveEmail(){//true if has and false if doesn't
        return !(email == null);
    }
}
