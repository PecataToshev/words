package com.words.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.speech.RecognizerIntent;
import android.widget.Toast;

import com.words.MainActivity;
import com.words.R;
import com.words.users.UserLoginRegister;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

public class BasicFunctions {
    public static void dialogMessage(Activity activity, String alertMessage){
        String okMessage = activity.getString(R.string.OK);
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(alertMessage)
                .setCancelable(false)
                .setPositiveButton(okMessage, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public static void toast(Activity activity, String text){
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(activity, text, duration);
        toast.show();
    }

    public static SharedPreferences sharedPrefs(Activity activity){
        return activity.getSharedPreferences("com.words", Context.MODE_PRIVATE);
    }

    public static SharedPreferences.Editor prefsEditor (Activity activity){
        return sharedPrefs(activity).edit();
    }

    public static int getNumberOfList (Activity activity){
        return sharedPrefs(activity).getInt(activity.getString(R.string.pref_listCount), 0);
    }

    public static String getUsername(Activity activity){
        return sharedPrefs(activity).getString(activity.getString(R.string.pref_username), "");
    }

    public static String getEmail(Activity activity){
        return sharedPrefs(activity).getString(activity.getString(R.string.pref_email), "");
    }

    public static int getTurnTime(Activity activity){
        return sharedPrefs(activity).getInt(activity.getString(R.string.pref_turnTime), 30);
    }

    public static boolean getAddUsername(Activity activity){
        return sharedPrefs(activity).getBoolean(activity.getString(R.string.pref_addName), false);
    }

    public static int numberOfMatchesInString(String string, String matchingItem){
        return (string.length() - string.replace(matchingItem, "").length());
    }

    public static boolean nameContainsEmail(String playerName){
        if(playerName.contains("#")){
            String _email = playerName.substring(0, playerName.indexOf("#"));
            if(_email.isEmpty()){
                return false;
            }

            if(UserLoginRegister.checkEmail(_email)){
                return true;
            }
        }

        return false;
    }

    public static String getNameWithoutEmail(String playerName) {
        return playerName.substring(playerName.indexOf("#") + 1);
    }

    public static ArrayList<String> getStringToArrList(String str){
        str = str.replaceAll("[\\[\\]]", "");
        ArrayList<String> p = new ArrayList<String>(Arrays.asList(str.split(",")));

        for(int i = 0;  i < p.size();  i++){
            p.set(i, removeHomeAndEndSpaces(p.get(i)));
        }

        return p;
    }

    public static void startNewActivity(Activity activity, Class starting){
        Intent intent = new Intent(activity, starting);
        activity.startActivity(intent);
        activity.finish();
    }

    public static void toMainMenuDialog(final Activity activity){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        builder.setMessage(R.string.toMainMenuExitGame);

        builder.setPositiveButton(R.string.Yes, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                toMainMenu(activity);
            }

        });

        builder.setNegativeButton(R.string.No, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    public static void toMainMenu(final Activity activity){
        startNewActivity(activity, MainActivity.class);
    }

    public static void exitApp(Activity activity){
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
    }

    public static String removeHomeAndEndSpaces(String text){
        boolean cont = false;
        for(int i = 0;  i < text.length();  i++){
            if(text.charAt(i) != ' '){
                cont = true;
                break;
            }
        }

        if(cont){
            while(text.charAt(0) == ' '){
                text = text.substring(1);
            }

            while(text.charAt(text.length() - 1) == ' '){
                if(text.length() - 2 > 0 ){
                    text = text.substring(0, text.length() - 1);
                }

            }

        } else {
            text = "";
        }

        return text;
    }

    public static char lastLetter(String text){
        return text.charAt(text.length() - 1);
    }

    public static String lastLetterString(String text){
        return Character.toString(lastLetter(text));
    }

    public static String[] allBulAlphabet(){
        return new String[]{"а","б","в","г","д","е","ж","з","и","й","к","л","м","н","о","п","р","с",
                "т","у","ф","х","ц","ч","ш","щ","ъ","ь","ю","я"};
    }

    public static String URL(){
        return "http://petartoshev.ddns.cablebg.net:80/Words_ConnectToDB/";
    }

    public static String getResult(String requestURL){
        try {
            URL url = new URL(requestURL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            return readInputStream(conn.getInputStream());

        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    private static String readInputStream(InputStream in) throws IOException {
        StringBuilder  sb = new StringBuilder();
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String read;

        while((read=br.readLine()) != null) {
            sb.append(read);
        }

        br.close();
        return sb.toString();
    }

    public static boolean isConectedWithInternet(Activity activity){
        ConnectivityManager cm =
                (ConnectivityManager)activity.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return (activeNetwork != null && activeNetwork.isConnectedOrConnecting());
    }

    public static void promptSpeechInput(Activity activity, int RESULT_SPEECH) {
        String languagePref = "bg";
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, languagePref);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, languagePref);
        intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);
        intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, "com.words");

        try {
            activity.startActivityForResult(intent, RESULT_SPEECH);
        } catch (ActivityNotFoundException a) {
            BasicFunctions.toast(activity, activity.getString(R.string.voiceInputIsntSupported));
        }
    }
}
