package com.words.utils;

public class WordsContainer {
    private String word;
    private int id;

    public WordsContainer(String word, int id) {
        this.word = word;
        this.id = id;
    }

    public String getWord() {
        return word;
    }

    public int getId() {
        return id;
    }

}
