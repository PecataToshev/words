package com.words;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.words.game.LocalGameActivity;
import com.words.utils.BasicFunctions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class InputNamesLocalActivity extends AppCompatActivity {
    private Button doneBut;
    private Button clearBut;
    private EditText inputName;
    private LinearLayout nameContainer;
    private Button addName;
    private ArrayList <String> namesList;
    private Boolean playComputer;
    private Button listShowHideButton;
    private boolean listButtonsAreVisible;
    private Button saveList;
    private Button importList;
    private ImageButton buttonSpeech;
    private int computerLvl;
    private final int RESULT_SPEECH = 0;
    private final int REQUEST_LIST_ID = 1;

    private void initialise(boolean compPlay){
        namesList = new ArrayList <String>();

        inputName = (EditText) findViewById(R.id.inputName);
        doneBut = (Button) findViewById(R.id.doneBut);
        clearBut = (Button) findViewById(R.id.clearBut);
        nameContainer = (LinearLayout) findViewById(R.id.nameContainer);
        addName = (Button) findViewById(R.id.plusBut);
        saveList = (Button) findViewById(R.id.buttonSaveList);
        importList = (Button) findViewById(R.id.buttonImportList);
        listShowHideButton = (Button) findViewById(R.id.buttonList);
        buttonSpeech = (ImageButton) findViewById(R.id.voiceInputNames);

        listButtonsAreVisible = false;
        inputName.setText("");
        hideLists();
        playComputer = compPlay;
        computerLvl = 1;
    }

    private void hideLists(){
        saveList.setVisibility(View.GONE);
        importList.setVisibility(View.GONE);
    }

    private void showLists(){
        saveList.setVisibility(View.VISIBLE);
        importList.setVisibility(View.VISIBLE);
    }

    public void setComputerLvl(int _computerLvl) {
        computerLvl = _computerLvl;
    }

    private void getComputerLvl(){
        final CharSequence[] items = {getString(R.string.Easy),getString(R.string.Medium),
                getString(R.string.Hard),getString(R.string.Extreme),getString(R.string.Impossible)};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.selectComputerLvl);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case 0: case 1: case 2: case 3: case 4:
                        setComputerLvl(which + 1);
                        break;
                    default:
                        setComputerLvl(1);
                        break;
                }
                dialog.dismiss();
                startGame();
            }
        }).show();
    }

    private boolean existComputer(){
        for(int i = 0;  i < namesList.size();  i++){
            if(namesList.get(i).equals(getString(R.string.Computer))){
                return true;
            }
        }
        return false;
    }

    private void ifInputComputer(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.InputNameComputer);
        builder.setPositiveButton(R.string.Yes, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                addName(getString(R.string.Computer));
                playComputer = true;
            }

        });

        builder.setNegativeButton(R.string.No, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private void namesToArrList(String str){
        ArrayList<String> p = BasicFunctions.getStringToArrList(str);

        for(int i = 0;  i < p.size();  i++){
            p.set(i, BasicFunctions.removeHomeAndEndSpaces(p.get(i)));
        }

        namesList = p;
    }

    private void addToTextView(String text, int ind){
        TextView nameShow = new TextView(InputNamesLocalActivity.this);
        nameShow.setText((ind + 1) + ". " + text);
        nameShow.setLayoutParams(new
                ViewGroup.LayoutParams
                (ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        nameContainer.addView(nameShow, 0);
    }

    private void printAllNamesList(){
        for (int ind = 0; ind < namesList.size(); ind++) {
            addToTextView(namesList.get(ind).toString(), ind);
        }
    }

    private void addName(String name){
        namesList.add(name);

        if(BasicFunctions.nameContainsEmail(name)){
            name = BasicFunctions.getNameWithoutEmail(name);
        }

        addToTextView(name, namesList.size() - 1);
        inputName.setText("");
    }

    private void checkName(){
        String name = inputName.getText().toString();
        name = BasicFunctions.removeHomeAndEndSpaces(name);

        if(name.equals("")){
            inputName.setText("");
            return;
        }

        if(name.contains(",")){
            BasicFunctions.dialogMessage(this, getString(R.string.CannotContainsCommas));
            return;
        }

        if(name.equals(getString(R.string.Computer)) && !playComputer){
            if(namesList.size() > 0){
                ifInputComputer();
            } else {
                BasicFunctions.dialogMessage(this, getString(R.string.CannotStartWithComputerTurn));
                inputName.setText("");
            }
            return;
        }

        if(name.equals(getString(R.string.Computer)) && playComputer){
            BasicFunctions.dialogMessage(this, getString(R.string.moreThanOneComputer));
            inputName.setText("");
            return;
        }

        if(namesList.contains(name)){
            BasicFunctions.dialogMessage(this, getString(R.string.existingPlayer));
            return;
        }

        addName(name);
    }

    private void startGame(){
        Intent localGame = new Intent(InputNamesLocalActivity.this, LocalGameActivity.class);
        localGame.putExtra(getString(R.string.activityExtra_names), namesList.toString());
        localGame.putExtra(getString(R.string.activityExtra_compLvl), computerLvl);
        startActivity(localGame);
        finish();
    }

    private void gameIsReady(){
        if(namesList.size() + (playComputer? 1: 0) > 1) {
            if (playComputer && !existComputer()) {
                namesList.add(getString(R.string.Computer));
            }

            if(existComputer()){
                getComputerLvl();
            } else {
                startGame();
            }

        } else if(playComputer) {
            BasicFunctions.dialogMessage(this, getString(R.string.atLeast1Player));
        } else {
            BasicFunctions.dialogMessage(this, getString(R.string.atLeast2Players));
        }

    }

    private void putNamelist(String listName){
        JSONObject jo = new JSONObject();
        try {
            jo.put(getString(R.string.list_listName), listName);
            jo.put(getString(R.string.list_playersList), namesList.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        SharedPreferences.Editor editor = BasicFunctions.prefsEditor(this);
        editor.putInt(getString(R.string.pref_listCount), (BasicFunctions.getNumberOfList(this) + 1));
        editor.apply();

        editor = BasicFunctions.prefsEditor(this);
        editor.putString(getString(R.string.pref_forListNumber) + BasicFunctions.getNumberOfList(this), jo.toString());
        editor.apply();

        BasicFunctions.toast(this, "Списъкът бе запазен успешно.");
    }

    private void getAndAddNamesFromList(){
        ArrayList<String> addingName = new ArrayList<>();
        try {
            String result = BasicFunctions.sharedPrefs(this).
                    getString(getString(R.string.pref_forListNumber) + BasicFunctions.getNumberOfList(this), null);

            JSONObject jo = new JSONObject(result);
            addingName = BasicFunctions.getStringToArrList(jo.getString(getString(R.string.list_playersList)));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        String existingName = "";

        for(int i = 0;  i < addingName.size();  i++){
            if(namesList.contains(addingName.get(i))){
                existingName += ", " + addingName.get(i);
            } else {
                addName(addingName.get(i));
            }

        }

        if(existingName.length() > 3){
            String dialogMessageText = "";
            existingName = existingName.substring(2);

            if(BasicFunctions.numberOfMatchesInString(existingName, ",") > 0){
                dialogMessageText += "Имената " + existingName + " вече са били въведени!";
            } else {
                dialogMessageText += "Името " + existingName + " вече е било въведено!";
            }

            BasicFunctions.dialogMessage(InputNamesLocalActivity.this, dialogMessageText);
        }

    }

    @Override
    public void onBackPressed(){
        BasicFunctions.startNewActivity(this, SelectGameTypeActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_name_local);

        initialise(getIntent().getExtras().getBoolean(getString(R.string.activityExtra_playsComputer)));

        listShowHideButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listButtonsAreVisible){
                    hideLists();
                } else{
                    showLists();
                }

                listButtonsAreVisible = !listButtonsAreVisible;
            }
        });

        buttonSpeech.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BasicFunctions.promptSpeechInput(InputNamesLocalActivity.this, RESULT_SPEECH);
            }
        });

        addName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkName();
            }
        });

        importList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(BasicFunctions.getNumberOfList(InputNamesLocalActivity.this) > 0) {
                    getAndAddNamesFromList();
                } else {
                    BasicFunctions.toast(InputNamesLocalActivity.this, "Нямате запазени списъци.");
                }

            }
        });

        saveList.setOnClickListener(new View.OnClickListener() {
            String nameToBePutted;
            @Override
            public void onClick(View v) {
                if(namesList.size() < 2){
                    BasicFunctions.toast(InputNamesLocalActivity.this, "Трябва да има поне две въведени имена, за да може да бъде запазен!");
                    return;
                }

                nameToBePutted = getString(R.string.pref_forListNumber)
                        + BasicFunctions.getNumberOfList(InputNamesLocalActivity.this);

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(InputNamesLocalActivity.this);
                alertDialog.setTitle("Име на списъка");
                alertDialog.setMessage("Въведете желаното име за списъка");

                final EditText input = new EditText(InputNamesLocalActivity.this);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                input.setLayoutParams(lp);
                alertDialog.setView(input);

                alertDialog.setPositiveButton("Задай",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                String listName = input.getText().toString();
                                listName = BasicFunctions.removeHomeAndEndSpaces(listName);
                                if(listName.length() > 0){
                                    nameToBePutted = listName;
                                }

                                dialog.dismiss();
                                putNamelist(nameToBePutted);
                            }
                        });

                alertDialog.setNegativeButton("Име по подразбиране",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                putNamelist(nameToBePutted);
                            }
                        });

                alertDialog.show();
            }

        });

        clearBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(namesList.size() > 0) {
                    namesList.remove(namesList.size() - 1);
                    nameContainer.removeAllViews();
                    printAllNamesList();
                }
            }
        });

        doneBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gameIsReady();
            }
        });

        if(BasicFunctions.getAddUsername(this)){
            inputName.setText(BasicFunctions.getUsername(this));
            addName.performClick();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_LIST_ID && resultCode == RESULT_OK  && null != data) {

            return;
        }

        if (requestCode == RESULT_SPEECH && resultCode == RESULT_OK && null != data){
            ArrayList<String> names = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            inputName.setText(names.get(0));
            addName.performClick();
        }
    }

    public void onSaveInstanceState(Bundle outState){
        outState.putString("nameList", namesList.toString());
        outState.putString("inputText", inputName.getText().toString());
        outState.putBoolean("playComp", playComputer);
        super.onSaveInstanceState(outState);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        namesToArrList(savedInstanceState.getString("nameList"));
        inputName.setText(savedInstanceState.getString("inputText"));
        playComputer = savedInstanceState.getBoolean("playComp");

        printAllNamesList();
    }
}
